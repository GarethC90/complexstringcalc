# Complex String Calculator # 

You can use any version of Java but you can only use standard Java libraries. Please commit regularly to the provided Git repository. 

The use of the following is forbidden:
-	ScriptEngine
-	Shunting-yard algorithm
-	Third party libraries

## Part 1 ##
Write a program, using TDD methods, that can perform integer calculations on arbitrarily complex String expressions using addition, subtraction, multiplication and division operators (with normal precedence rules) plus parenthesis for grouping.

**Example:**
“2+(2*4)” = 10 
“21+3*6/3+2*2/4–3*1+6” = 31 
“21+18/3+4/4–3+6–3” = 28

## Part 2 ##
Modify your calculator to be a calculator webservice

**Example Output:**
$ curl -X PUT -d '(1+2)/3' localhost:6666/calculate 
1 

$ curl -X PUT -d '(1+2+3+4)/3+1' localhost:6666/calculate 
4 

$ curl -X PUT -d '10+2*3' localhost:6666/calculate
16 

## Part 3 ##
Modify the calculator service to add an additional endpoint that accepts and returns JSON data. 

**Example Output:**
$ curl -X PUT -d '{ lhs: { lhs: 1, rhs: 2, operator: “+”}, rhs: 3, operator: “/” }' localhost:6666/calculate.json
1
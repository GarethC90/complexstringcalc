package com.ericsson.stringcalculator.services.ejb;

import java.util.Arrays;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Local;
import javax.ejb.Stateless;

import com.ericsson.stringcalculator.services.StringCalculatorService;

@Local
@Stateless
public class StringCalculatorServiceEJB implements StringCalculatorService {


	private static Set<String> illegalCharacterCombinations;

	static {
		illegalCharacterCombinations = new HashSet<String>();
		illegalCharacterCombinations.add("++");
		illegalCharacterCombinations.add("+--");
		illegalCharacterCombinations.add("+*");
		illegalCharacterCombinations.add("+/");
		illegalCharacterCombinations.add("-+");
		illegalCharacterCombinations.add("---");
		illegalCharacterCombinations.add("-*");
		illegalCharacterCombinations.add("-/");
		illegalCharacterCombinations.add("*+");
		illegalCharacterCombinations.add("*--");
		illegalCharacterCombinations.add("**");
		illegalCharacterCombinations.add("*/");
		illegalCharacterCombinations.add("/+");
		illegalCharacterCombinations.add("/--");
		illegalCharacterCombinations.add("/*");
		illegalCharacterCombinations.add("//");
		illegalCharacterCombinations.add("/)");
		illegalCharacterCombinations.add("*)");
		illegalCharacterCombinations.add("-)");
		illegalCharacterCombinations.add("+)");
	}

	/**
	 * Returns the String representation of an Integer whose value is equal to the input equation. 
	 * 
	 * Performs integer calculations on arbitrarily complex String expressions using addition, 
	 * subtraction, multiplication and division operators (with normal precedence rules) plus parenthesis for grouping.
	 * 
	 * @param	A String representation of a valid Integer equation. 
	 * @return	A String representation of an Integer whose value is equal to the input equation. 
	 * @throws	IllegalArgumentException if the input contains illegal characters or unbalanced parentheses.  
	 */
	public String calculate(String expression) {

		expression = checkForIllegalCharacterCombinations(expression);

		if(expression.contains("(") || expression.contains(")")) {
			return evaluateExpressionInBrackets(new StringBuilder(expression));
		}

		else if(expression.contains("*") || expression.contains("/")) {
			return multiplyAndDivide(splitOperatorsAndOperands(expression));
		} 

		else if(expression.contains("+") || expression.contains("-")) {
			return addAndSubtract(splitOperatorsAndOperands(expression));
		}

		return expression;
	}


	private String checkForIllegalCharacterCombinations(String expression) {

		String regex = "[^*+/\\-\\(\\)0-9]";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(expression);

		if(matcher.find()) {
			throw new IllegalArgumentException(expression + " contains illegal characters");
		}

		if(expression.startsWith("*") || expression.startsWith("/")) {
			throw new IllegalArgumentException(expression + ": begins with a * or a /");
		}

		for (String illegalCharCombo : illegalCharacterCombinations)
		{
			if(expression.contains(illegalCharCombo)){
				throw new IllegalArgumentException(expression + " contains illegal character combination: " + illegalCharCombo);
			}
		}

		expression.replaceAll("--", "+");

		if(expression.startsWith("+")) {
			expression = expression.substring(1);
		}

		return expression;
	}


	private String evaluateExpressionInBrackets(StringBuilder expression) {

		List<Integer> openingBracketsStack = new Stack<Integer>();

		for(int i = 0; i < expression.length(); i++) {
			if(expression.charAt(i) == '(') {
				openingBracketsStack.add(i);
				if(isMultiplicationSignNeededBeforeBracket(expression, i)) {
					expression = expression.insert(i, "*");
				}
			}
			else if(expression.charAt(i) == ')') {
				if(isMultiplicationSignNeededAfterBracket(expression, i)) {
					expression = expression.insert(i+1, "*");
				}

				int openingBracketIndex;	
				try {
					openingBracketIndex = ((Stack<Integer>)  openingBracketsStack).pop();
				} catch (EmptyStackException e) {
					throw new IllegalArgumentException("No opening bracket to match closing bracket at index: " +i+ "in expression: " +expression.toString());
				}

				int closingBracketIndex = i;
				String result = calculate(expression.subSequence(openingBracketIndex+1, closingBracketIndex).toString());
				return calculate(expression.replace(openingBracketIndex, closingBracketIndex+1, result).toString());
			}
		}	

		throw new IllegalArgumentException("Brackets do not match in expression: " + expression);
	}


	private boolean isMultiplicationSignNeededBeforeBracket(StringBuilder expression, int index) {
		if((index-1) != -1) {
			if(Character.isDigit(expression.charAt(index-1))) {
				return true;
			}
		}

		return false;
	}


	private boolean isMultiplicationSignNeededAfterBracket(StringBuilder expression, int index) {
		if((index+1) != expression.length()) {
			if(Character.isDigit(expression.charAt(index+1))) {
				return true;
			}
		}

		return false;
	}


	private String multiplyAndDivide(OperandsAndOperators expression) {

		expression = performOperations(expression, Operation.TIMES, Operation.DIVIDE);
		expression = removeProcessedMultiplyAndDivideSigns(expression);

		if(isAdditionAndSubtractionStepNeeded(expression)) {
			return addAndSubtract(expression);
		}	

		return expression.getResult();
	}


	private OperandsAndOperators removeProcessedMultiplyAndDivideSigns(OperandsAndOperators expression) {
		Pattern pattern = Pattern.compile("\\*|/");
		Matcher matcher = pattern.matcher(expression.operators);
		expression.operators = new StringBuilder(matcher.replaceAll(""));
		return expression;
	}


	private boolean isAdditionAndSubtractionStepNeeded(OperandsAndOperators expression) {
		if(expression.operators.toString().contains("+") || expression.operators.toString().contains("-")) {
			return true;
		}	

		return false;
	}


	private String addAndSubtract(OperandsAndOperators expression) {
		if(isExpressionOneNegativeNumber(expression)) {
			return expression.getResult();
		}

		expression = performOperations(expression, Operation.PLUS, Operation.MINUS);

		return expression.getResult();
	}


	private boolean isExpressionOneNegativeNumber(OperandsAndOperators expression) {
		if(expression.operators.length() == 0) {
			return true ;
		}

		return false;
	}


	private OperandsAndOperators performOperations(OperandsAndOperators expression, Operation operatorOne, Operation operatorTwo) {

		List<Integer> indicesOfOperandsToBeRemoved = new LinkedList<Integer>();

		for(int i = 0; i < expression.operators.length(); i++) {
			String operatorSymbol = String.valueOf(expression.operators.charAt(i));

			if(operatorSymbol.equals(operatorOne.getOperationSymbol()) || operatorSymbol.equals(operatorTwo.getOperationSymbol())) {
				Integer operandOne = Integer.parseInt(expression.operands.get(i));
				Integer operandTwo = Integer.parseInt(expression.operands.get(i+1));
				Integer result = Operation.fromSymbol(operatorSymbol).apply(operandOne, operandTwo);
				expression.operands.set(i+1, String.valueOf(result));
				indicesOfOperandsToBeRemoved.add(i);
			}
		}

		return removeProcessedOperands(expression, indicesOfOperandsToBeRemoved);	
	}


	private OperandsAndOperators splitOperatorsAndOperands(String expression) {

		List<String> operands;
		String regex = "\\*|/|\\+|\\-";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(expression);

		if(matcher.find()) {
			operands = new LinkedList<String>(Arrays.asList(expression.split(regex)));
		}
		else { 
			throw new IllegalArgumentException("Regex: " +regex+ " does not match regular expression: " + expression);
		}

		String operators = removeNumbersFromStringLeavingOperators(expression);

		if(doesExpressionContainNegativeNumbers(operands)) { 
			return dealWithNegativeNumbers(operands, new StringBuilder(operators));	
		} 

		return new OperandsAndOperators(operands, new StringBuilder(operators));
	}


	private String removeNumbersFromStringLeavingOperators(String expression) {
		return expression.replaceAll("[0-9]", "");
	}


	private boolean doesExpressionContainNegativeNumbers(List<String> operands) {
		if(operands.contains("")) { 
			return true;
		}

		return false;
	}


	private OperandsAndOperators dealWithNegativeNumbers(List<String> operands, StringBuilder operators) {

		List<Integer> negNumIndices = getIndicesOfNegativeNumbers(operands);

		operands = removeEmptyListElementsCausedByNegativeNumbers(operands);

		int offset = 0;
		for(Integer index: negNumIndices) {
			index = index - offset;
			String negatedNumber = String.valueOf(-1*Integer.parseInt(operands.get(index)));
			operands.set(index, negatedNumber);
			operators.deleteCharAt(index);
			offset++;
		}

		return new OperandsAndOperators(operands, operators);
	}


	private List<Integer> getIndicesOfNegativeNumbers(List<String> operands) {
		List<Integer> negNumPosition = new LinkedList<Integer>();

		for(Integer i = 0; i < operands.size(); i++) {
			if(operands.get(i).equals("")) {
				negNumPosition.add(i);
			}
		}	

		return negNumPosition;
	}


	private List<String> removeEmptyListElementsCausedByNegativeNumbers(List<String> operands) {
		operands.removeAll((Collections.singleton("")));
		return operands;
	}


	private OperandsAndOperators removeProcessedOperands(OperandsAndOperators expression, List<Integer> indicesOfOperatorsToBeRemoved) {
		int offset = 0;
		for(Integer index : indicesOfOperatorsToBeRemoved) {
			expression.operands.remove(index-offset);
			offset++;
		}

		return expression;
	}


	private static class OperandsAndOperators {
		List<String> operands;
		StringBuilder operators;

		private OperandsAndOperators(List<String> operands, StringBuilder operators) {
			this.operands = operands;
			this.operators = operators;
		}

		private String getResult() {
			return operands.get(0);
		}

	}


	private static enum Operation {
		PLUS("+"){
			Integer apply(Integer x, Integer y) { return x + y; }
		},
		MINUS("-"){
			Integer apply(Integer x, Integer y) { return x - y; }
		},
		TIMES("*"){
			Integer apply(Integer x, Integer y) { return x * y; }
		},
		DIVIDE("/"){
			Integer apply(Integer x, Integer y) { return x / y; }
		};

		private final String symbol;

		private Operation(String symbol) {	
			this.symbol = symbol; 
		}

		private String getOperationSymbol() { 
			return symbol; 
		}

		private static final Map<String, Operation> symbolToEnum = new HashMap<String, Operation>();

		static {
			for(Operation op : values()) {
				symbolToEnum.put(op.symbol, op);
			}
		}

		private static Operation fromSymbol(String symbol) {
			return symbolToEnum.get(symbol);
		}

		abstract Integer apply(Integer x, Integer y);
	}

}

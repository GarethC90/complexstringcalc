package com.ericsson.stringcalculator.services;

import javax.ejb.Local;

@Local
public interface StringCalculatorService {

	String calculate(String expression);
		
}

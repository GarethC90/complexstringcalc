package com.ericsson.stringcalculator.rest;

import java.util.LinkedHashMap;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;

import com.ericsson.stringcalculator.services.StringCalculatorService;


@Path("/calculate")
public class StringCalculatorREST {

	@EJB
	private StringCalculatorService stringCalculatorService;

	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response calculate(@FormParam("id") String id) {
		String result =  stringCalculatorService.calculate(id);
		return Response.status(200).entity("The Result is: " + result).build();
	}

	@SuppressWarnings("unchecked")
	@PUT
	@Path("/json")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response calculateJSON(JSONObject obj) {
		String lhs;
		String rhs;
		String operator;
		
		if(obj.get("lhs") instanceof LinkedHashMap) {
			lhs = processJSONObject((LinkedHashMap<?, ?>) obj.get("lhs"));
		}
		else {
			lhs = (String) obj.get("lhs");
		}
		if(obj.get("rhs") instanceof LinkedHashMap) {
			rhs = processJSONObject((LinkedHashMap<?, ?>) obj.get("rhs"));
		}
		else {
			rhs = (String) obj.get("rhs");
		}
		operator = (String) obj.get("operator");
		
		String result =  stringCalculatorService.calculate(lhs + operator + rhs);	
		JSONObject jsonResultObj = new JSONObject();
		jsonResultObj.put("result", result);
		return Response.status(200).entity(jsonResultObj).build();
	}

	private String processJSONObject(LinkedHashMap<?, ?> obj) {
		String lhs;
		String rhs;
		String operator;
		
		if(obj.get("lhs") instanceof LinkedHashMap) {
			lhs = processJSONObject((LinkedHashMap<?, ?>) obj.get("lhs"));
		}
		else {
			lhs = (String) obj.get("lhs");
		}
		if(obj.get("rhs") instanceof LinkedHashMap) {
			rhs =processJSONObject((LinkedHashMap<?, ?>) obj.get("rhs"));
		}
		else {
			rhs = (String) obj.get("rhs");
		}
		operator = (String) obj.get("operator");
		return lhs + operator + rhs;
	}
}


package com.ericsson.stringcalculator.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ericsson.stringcalculator.StringCalculator;
import com.ericsson.stringcalculator.StringCalculatorAltImpl;


public class StringCalculatorTest {

	private StringCalculator sc = new StringCalculatorAltImpl();

	@Test
	public void one() {
		String result = sc.calculate("1");
		assertEquals(result, "1");
	}

	@Test
	public void ten() {
		String result = sc.calculate("10");
		assertEquals(result, "10");
	}

	@Test
	public void nineDivThree() {
		String result = sc.calculate("9/3");
		assertEquals("3", result);
	}

	@Test
	public void oneHundredDivTenDivTwo() {
		String result = sc.calculate("1000/10/2");
		assertEquals("50", result);
	}

	@Test
	public void tenTimeFiftyDivTwo() {
		String result = sc.calculate("10*50/2");
		assertEquals("250", result);
	}

	@Test
	public void tenTimeFiftyDivFourPlusThree() {
		String result = sc.calculate("10*50/4+3");
		assertEquals("128", result);
	}

	@Test
	public void thirtyThreeTimesThree() {
		String result = sc.calculate("33*3");
		assertEquals("99", result);
	}

	@Test
	public void tenTimeFiftyDivFourPlusThreeMinusNine() {
		String result = sc.calculate("10*50/4+3-9");
		assertEquals("119", result);
	}

	@Test
	public void twoPlusThreeMinusTwoTimesFourMinusFive() {
		String result = sc.calculate("2+3-2*4-5");
		assertEquals("-8", result);
	}

	@Test
	public void sixTimes5Times4Times3Times2() {
		String result = sc.calculate("6*5*4*3*2");
		assertEquals("720", result);
	}

	@Test
	public void sixMinus2Plus3Minus2Times4Minus5() {
		String result = sc.calculate("6-2+3-2*4-5");
		assertEquals("-6", result);
	}

	@Test
	public void sixMinus2Plus3Times4() {
		String result = sc.calculate("6-2+3*4");
		assertEquals("16", result);
	}

	@Test
	public void negSixMinus2Plus3Times4() {
		String result = sc.calculate("-96-2+3*4");
		assertEquals("-86", result);
	}

	@Test
	public void bracketsNegSixMinus2Plus3Times4() {
		String result = sc.calculate("(-96-2+3*4)");
		assertEquals("-86", result);
	}

	@Test
	public void twoBracketsNegSixMinus2Plus3Times4() {
		String result = sc.calculate("(-96+(2+3*4))");
		assertEquals("-82", result);
	}

	@Test
	public void twoBracketsNegSixMinus2Plus3Times4Plus55() {
		String result = sc.calculate("(-96+(2+3*4)+55)");
		assertEquals("-27", result);
	}

	@Test
	public void twoBracketsNegSixMinusBracket2Plus3Times4Plus55() {
		String result = sc.calculate("(-96+((2+3*4))+55)");
		assertEquals("-27", result);
	}

	@Test
	public void bracketBracketBracketNegTwoPlusThreeBracketMinusFourTimesFiveBracketPlusBracketBracketMinusFivePlusTowBracketTimesOneHundredBracketMinusFourHundred() {
		String result = sc.calculate("(((-2+3)-4)*5)+((-5+2)*100)-400");
		assertEquals("-715", result);
	}	

	@Test
	public void fortyEightDivTwoTimesBrackNinePlusThreeBrackMinusFourHundredPlusThreeTimesBrackMinusTwoBrack() {
		String result = sc.calculate("48/2*(9+3)-400+3*(-2)");
		assertEquals("-118", result);
	}

	@Test
	public void fortyEightDivTwoTimesBrackNinePlusThreeBrackMinusFourHundredPlusThreeTimesBrackMinusTwoBrackPlusBrackMinusThreeBrack() {
		String result = sc.calculate("48/2*(9+3)-400+3*(-2)+(-3)");
		assertEquals("-121", result);
	}

	@Test
	public void fortyEightDivTwoTimesBrackNinePlusThreeBrackMinusFourhundredPlusBrackMinusEightyBrackEtc() {
		String result = sc.calculate("48/2*(9+3)-400+(-80)+3*(-2)+(-3)");
		assertEquals("-201", result);
	}

	@Test
	public void fortyEightDivTwoTimeBrackETC() {
		String result = sc.calculate("48/2*(9+3)-400+(-80)+3*(-2)+(-3)/(-2)");
		assertEquals("-197", result);
	}

	@Test
	public void fortyEightDivTwoTimeBrackNinePlusThreeBrackMinusFourhundredPlus() {
		String result = sc.calculate("48/2*(9+3)-400+(-80)+3*(-2)+(-3)/(-2)-64");
		assertEquals("-261", result);
	}

	@Test
	public void fortyEightDivTwoBracketNinePlusThreeBracketMinusFourHundredPlusThreeTimesMinusTwo() {
		String result = sc.calculate("48/2(9+3)-400+3*-2");
		assertEquals("-118", result);
	}

	@Test
	public void brackNinePlusThreeBrackBrackEighteenBrack() {
		String result = sc.calculate("(9+3)(18)");
		assertEquals("216", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void illegalLettersTest() {
		sc.calculate("(9+a)(18)");
	}

	@Test(expected = IllegalArgumentException.class)
	public void unclosedBracketsTest() {
		sc.calculate("(9+18");
	}

	@Test
	public void bracket2Plus3BracketEight() {
		String result = sc.calculate("(2+3)8");
		assertEquals("40", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void twoPlusThreeBrackEight() {
		sc.calculate("2+3)8");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twoPlusQuoteBrackEight() {
		sc.calculate("2+\")8");
	}

	@Test(expected = IllegalArgumentException.class)
	public void star9() {
		sc.calculate("*9");
	}

	@Test(expected = IllegalArgumentException.class)
	public void div9() {
		sc.calculate("/9");
	}

	@Test()
	public void twelveMinusMinusThree() {
		String result = sc.calculate("12--3");
		assertEquals("15", result);
	}

	@Test()
	public void twelvePlusMinusThree() {
		String result = sc.calculate("12+-3");
		assertEquals("9", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelvePlusPlusThree() {
		sc.calculate("12++3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelvePlusTimesThree() {
		sc.calculate("12+*3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelvePlusDivThree() {
		sc.calculate("12+/3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveMinusPlusThree() {
		sc.calculate("12-+3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveMinusTimesThree() {
		sc.calculate("12-*3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveMinusDivThree() {
		sc.calculate("12-/3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveTimesPlusThree() {
		sc.calculate("12*+3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveTimesTimesThree() {
		sc.calculate("12**3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveTimesDivThree() {
		sc.calculate("12*/3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveDivPlusThree() {
		sc.calculate("12/+3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveDivTimesThree() {
		sc.calculate("12/*3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveDivDivThree() {
		sc.calculate("12//3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelvePlusMinusMinusThree() {
		sc.calculate("12+--3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveMinusMinusMinusThree() {
		sc.calculate("12---3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveStarMinusMinusThree() {
		sc.calculate("12+--3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveDivMinusMinusThreePlusBrack() {
		sc.calculate("(12-3+)");
	}

	@Test(expected = IllegalArgumentException.class)
	public void brackTwelveMinusMinusThreeMinusBrack() {
		sc.calculate("(12--3-)");
	}

	@Test(expected = IllegalArgumentException.class)
	public void plusTwelveDivMinusMinusThree() {
		sc.calculate("+12--3*)");
	}

	@Test(expected = IllegalArgumentException.class)
	public void brackTwelveMinusMinusThreeDivBrack() {
		sc.calculate("(12--3/)");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelvePointThree() {
		sc.calculate("12.3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveAndThree() {
		sc.calculate("12&3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void twelveHashThree() {
		sc.calculate("12#3");
	}

	@Test()
	public void plusTwelveMinusThree() {
		String result = sc.calculate("+12-3");
		assertEquals("9", result);
	}

	@Test()
	public void minusMinusTwelveMinusMinusThree() {
		String result = sc.calculate("--12--3");
		assertEquals("15", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void brackPlusTwelveTimesSixPlusFour() {
		sc.calculate("(+12*6+4");
	}
	
	@Test()
	public void brackPlusTwelveTimesSixPlusFourBrack() {
		String result = sc.calculate("(+12*6+4)");
		assertEquals("76", result);
	}
	
}

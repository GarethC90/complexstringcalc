package com.ericsson.stringcalculator;

public interface StringCalculator {

	String calculate(String expression);
	
}
